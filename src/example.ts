//#1
type Hello = 'Hello';
type World = 'World';
//only Hello World
const concat = function (first: Hello, second: World): string {
  return `${first} ${second}`;
}
//any string
const concatAnyString = function (first: string, second: string): string {
  return `${first} ${second}`;
}

//#2
interface myHometaskValue {
  howIDoIt: string,
  simeArray: [string, string, number],
  withData: [{
    howIDoIt: string,
    simeArray: [string, number]
  }]
}

const myHometask: myHometaskValue = {
  howIDoIt: 'I Do It Wel',
  simeArray: ["string one", "string two", 1],
  withData: [{
    howIDoIt: "I Do It Wel",
    simeArray: ["string one", 23]
  }]
}

//#3
interface MyArray<T> {
  [N: number]: T;
  //no initialValue
  reduce(cb: (prevValue: T, curValue: T, curIndex: number, array: T[]) => T): T;
  //with initialValue
  reduce(cb: (prevsValue: T, curtValue: T, curIndex: number, array: T[]) => T, initialValue: T): T;
}

const testArray: MyArray<number> = [1, 2, 3];
let test = testArray.reduce((a, b) => a + b, 10);
let test2 = testArray.reduce((a, b) => a + b);
console.log(test, test2);
